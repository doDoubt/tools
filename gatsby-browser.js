// redirect to page
exports.onClientEntry = () => {
    if (window.location.pathname === '/') {
        const defaultRoute = '/en-us';
        const langs = ['tr-tr', 'en-gb', 'en-us'];
        const storedLang = localStorage.getItem('i18nextLng');

        if (!storedLang || !langs.find((x) => x === storedLang)) {
            window.location.pathname = defaultRoute;
        } else {
            window.location.pathname = `/${langs.find((x) => x === storedLang)}`;
        }
    }
};
