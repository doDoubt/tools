![doDoubt Tools](https://res.cloudinary.com/https-dodoubt-com/image/upload/v1617923787/tools/tools-logo-forgit.png)

# doDoubt Tools
doDoubt Tools is an open source, single page website (code name is ``23A``) built upon Gatsby framework to provide small widgets to the public.

The project has two (2) major proposed-outcomes. First one is sharing the source code to grow internally and externally. While internal growth can be achieved by recieving external reviews and comments, external growth can be achieved by helping small businesses and good people who needs access to source code.
Second outcome is providing limited but useful tools to public via website.

Website is being served at:
* [On Premise](http://tools.appdoubt.com/)
* [Netlify](https://dodoubt.netlify.app/)

## Getting Started

## Prerequisites
There is no prerequisite, other than a working NPM and Node environment.

On the top of node, WE highly recommend using Gatsby-CLI which can be achieved via running following NPM command.

=============================================
```shell
    npm install -g gatsby-cli
```
=============================================


## Installation
The application does not require any pre-installation, other than a internet connection to restore NPM packages.


## Project Structure
The project structure is based upon the regular Gatsby website boilerplate which can be replicated via gatsby new command.

=============================================
```shell
    gatsby new {project name}
```
=============================================

More information regarding to folder structure can be found [here](https://www.gatsbyjs.com/docs/reference/gatsby-project-structure/).

Regardless, source folder structure is given below.

* **components** contains react components.
* **enums** contains enumarations used among the project.
* **pageBase** and **pages** are twins for holding page data. 
While files under **pages** are for holding routes, **pageBase** files contains necessary code to render pages. Such segregation was not a absolute necessity but a semantic requirement to support 
multi-lingual website using ``gatsby-plugin-i18n`` plugin.
* **served** folder conatins static assets which were exported into built-artifacts.
* **trans** folder contains necessary configuration, assets and component to provide language support in the website.


## Tests
Mainly due to its nature of being a playground, as of now, we have developed no test.


## Deployment
As a matter choice, partly related to OUR hosting environment, OUR development team is using ftp publish method. VS Code is being selected as OUR IDE, [SFTP](https://github.com/liximomo/vscode-sftp) is being employed for publishing purposes.

In a regular cycle, following steps can be followed to publish changes to remote server.

* Run following build command. Please note that as dictated on package meta, this can be achieved via ``build`` command as well.

=============================================
```shell
    gatsby build
```
=============================================

* Having code is being transpiled, open ``Commands Palette``. Depending upon operating system, you can open the palette by using key combination of [Ctrl]+[Shift]+[p].

* Select ``SFTP: Sync Local -> Remote`` command to publish built artifacts to remote ftp server.

Following is a sample configuration JSON for FTP tool, employed by US.

=============================================
```javascript
    {
        "name": "Tools Ftp Publish",
        "context": "C:/git/tools/public",
        "host": "tools.appdoubt.com",
        "protocol": "ftp",
        "port": 0,
        "username": "toolsuser",
        "password": "Nu1&t19p.!ezk",
        "remotePath": "/",
        "uploadOnSave": false,
        "downloadOnOpen": false,
        "ignore": [".vscode", ".git", ".DS_Store"],
        "syncOption": {
            "delete": true,
            "skipCreate": false,
            "ignoreExisting": false,
            "update": true
        },
        "connectTimeout": 10000
    }
```
=============================================

This project also configured to support [Netlify Deployment](https://www.netlify.com/). Please refer to Netlify for configuration options. You can access to OUR Netlify site from [https://dodoubt.netlify.app/](https://dodoubt.netlify.app/)


## Entity Migration
Excluding GraphQL for the sake of the argument, this project does not hold any entity.


### Localization
Localization realted code is being stored at ``src\trans`` folder.

## Things to Accomplish
* Auto sitemap generation will be a great contribution.
* Offline support would be another great contribution.
* Polyfills (ie, ES6 support) can be added to  ``gatsby-browser.js``.
* GraphQL support of Gatsby is not being utilized at the best extend. And, it can be improved. 

## Additional Notes
We have no additional notes.

## License
**doDoubt** and **doDoubt Tools** names and logos are owned by of US. On the other hand, this project holds [0BSD License](https://opensource.org/licenses/0BSD) which allows everyone to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

It should be noted that doDoubt is not liable for loss of revenue or indirect, special, incidental, consequential, punitive, or exemplary damages, or damages for lost profits, revenues, business interruption, or loss of business information due to direct or indirect useage of this project.


## References
* [Online MD Editor](https://dillinger.io/). For no reason, WE like it!
* [Gatsby](https://www.gatsbyjs.com/). Read more about the fastest frontend for the modern web.
* [SFTP from iximomo](https://github.com/liximomo/vscode-sftp). FTP publish extension that we employed.
* [The Great Gatsby](https://www.imdb.com/title/tt1343092/). For movie and house party lovers.

---


[![Netlify Status](https://api.netlify.com/api/v1/badges/1987b302-aee3-425b-ada9-2ff98eb4e4b8/deploy-status)](https://app.netlify.com/sites/dodoubt/deploys)