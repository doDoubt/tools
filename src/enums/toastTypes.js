const ToastTypes = Object.freeze({
    success: 'success',
    error: 'error',
});

export default ToastTypes;
