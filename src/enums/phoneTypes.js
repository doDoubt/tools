const PhoneTypes = Object.freeze({
    appleWatch: 'appleWatch',
    galaxys8: 'galaxys8',
    galaxys8Blue: 'galaxys8Blue',
    imacPro: 'imacPro',
    iphonex: 'iphonex',
    iphone8: 'iphone8',
    iphone8Gold: 'iphone8Gold',
    ipadpro: 'ipadpro',
    pixel2xl: 'pixel2xl',
    pixel: 'pixel',
    pixelBlack: 'pixelBlack',
    pixelBlue: 'pixelBlue',
    surfaceStudio: 'surfaceStudio',
});

export default PhoneTypes;
