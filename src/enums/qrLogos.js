const QrLogos = Object.freeze({
    custom: 'custom',
    fb: 'fb',
    gezin: 'gezin',
    in: 'in',
    ins: 'ins',
    twt: 'twt',
});

export default QrLogos;
