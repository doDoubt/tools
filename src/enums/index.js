import PageKeys from './pageKeys';
import PhoneTypes from './phoneTypes';
import ToastTypes from './toastTypes';
import QrLogos from './qrLogos';
import QrShapes from './qrShapes';

const Enums = {
    PageKeys,
    PhoneTypes,
    ToastTypes,
    QrLogos,
    QrShapes,
};

export default Enums;
