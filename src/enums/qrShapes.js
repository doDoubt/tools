const QrShapes = Object.freeze({
    dots: 'dots',
    squares: 'squares',
});

export default QrShapes;
