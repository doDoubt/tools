const PageKeys = Object.freeze({
    index: 'index',
    barcodes: 'barcodes',
    notFound: 'notFound',
    phonePre: 'phonePre',
});

export default PageKeys;
