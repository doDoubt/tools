const _smallWindowWidth = 640;

const ScroolToTop = () => typeof window !== 'undefined' && window.scrollTo({
    top: 0,
    left: 0,
    behavior: 'smooth',
});

const ScroolToPosition = ({ top = 0, left = 0 }) => typeof window !== 'undefined' && window.scrollTo({
    top,
    left,
    behavior: 'smooth',
});

const GetBodyHeight = () => window.document.body.scrollHeight;

const IsSmallWidth = (windowSize) => windowSize <= _smallWindowWidth;

const IsSmallBody = () => window && window.innerWidth <= _smallWindowWidth;

export const WindowHelper = {
    GetBodyHeight,
    IsSmallWidth,
    IsSmallBody,
    ScroolToTop,
    ScroolToPosition,
};
