const Elipses = '…';

const GenerateUniqueId = () =>
    `${Date.now().toString(36)}.${Math.random().toString(36).substring(2, 6)}`;

export const StringHelper = {
    Elipses,
    GenerateUniqueId,
};
