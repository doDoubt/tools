import { navigate } from 'gatsby';

import { getAppCulture } from '../trans/transConfig';

const Page = () => typeof (window) !== 'undefined' && navigate(`/${getAppCulture()}`);

export default Page;
