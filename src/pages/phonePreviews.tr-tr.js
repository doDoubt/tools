import React from 'react';

import BasePage from '../pageBase/phonePreviews';

const Page = () => (
    <BasePage
        lang="tr-tr"
    />
);

export default Page;
