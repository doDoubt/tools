import React from 'react';

import BasePage from '../pageBase/index';

const Page = () => (
    <BasePage
        lang="en-gb"
    />
);

export default Page;
