import React from 'react';

import BasePage from '../pageBase/index';

const Page = () => (
    <BasePage
        lang="tr-tr"
    />
);

export default Page;
