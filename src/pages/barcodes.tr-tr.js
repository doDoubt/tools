import React from 'react';

import BasePage from '../pageBase/barcodes';

const Page = () => (
    <BasePage
        lang="tr-tr"
    />
);

export default Page;
