import React from 'react';

import BasePage from '../pageBase/barcodes';

const Page = () => (
    <BasePage
        lang="en-gb"
    />
);

export default Page;
