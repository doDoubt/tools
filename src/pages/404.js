import React from 'react';
import PropTypes from 'prop-types';
import {
    withTranslation,
} from 'react-i18next';

import Enums from '../enums';
import Layout from '../components/layout';

const _NotFoundPage = ({ t }) => (
    <Layout
        lang={t('lang')}
        pageKey={Enums.PageKeys.notFound}
    >
        <section className="pt-5 pb-5 mt-5">
            <div className="container h-100">
                <div className="row">
                    <div className="col-12 text-white">
                        <h1>404: {t('notfoundPage.notFound')}</h1>
                        <p>{t('notfoundPage.notFoundMsg')}</p>
                    </div>
                </div>
            </div>

        </section>
    </Layout>
);

_NotFoundPage.propTypes = {
    t: PropTypes.func.isRequired,
};

const NotFoundPage = withTranslation()(_NotFoundPage);

export default NotFoundPage;
