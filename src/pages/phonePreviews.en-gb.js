import React from 'react';

import BasePage from '../pageBase/phonePreviews';

const Page = () => (
    <BasePage
        lang="en-gb"
    />
);

export default Page;
