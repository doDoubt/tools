import React from 'react';
import PropTypes from 'prop-types';
import Flag from 'react-flagkit';

import { APP_LANGUAGES } from './transConfig';

const TransSelect = ({ lang }) => {
    const _handleChange = (code) => {
        if (code.toLowerCase() === lang) return;
        const paths = window.location.pathname && window.location.pathname.length > 1
            ? window.location.pathname.split('/')
            : [''];
        paths[1] = code;
        window.location.replace(paths.join('/'));
    };

    return (
        <span
            className="nav-link lang-select"
            style={{ marginTop: '-9px' }}
        >
            {
                APP_LANGUAGES.map((e, i) => (
                    <span key={i} className="pr-2">
                        <Flag
                            className={`cursor-point ${lang === e.val ? 'selectedLang' : ''}`}
                            country={e.code}
                            onClick={() => _handleChange(e.val)}
                        />
                    </span>
                ))
            }
        </span>
    );
};

TransSelect.propTypes = {
    lang: PropTypes.string.isRequired,
};

export default TransSelect;
