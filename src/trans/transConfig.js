import i18next from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

import transText from './transText';

const DEFAULT_CULTURE = 'en-us';

export const APP_LANGUAGES = Object.freeze([
    {
        val: 'tr-tr',
        code: 'TR',
        name: 'Türkçe',
    },
    {
        val: 'en-gb',
        code: 'GB',
        name: 'United Kingdom',
    },
    {
        val: 'en-us',
        code: 'US',
        name: 'United States',
    },
]);

const i18n = i18next.createInstance();

export const getAppCulture = () => {
    if (typeof window === 'undefined') return null;

    const culture = i18next.language || window.localStorage.i18nextLng;

    if (!culture) return APP_LANGUAGES.find((x) => x.val === DEFAULT_CULTURE);

    const item = APP_LANGUAGES.find((x) => x.val === culture);

    if (!item) return APP_LANGUAGES.find((x) => x.val === DEFAULT_CULTURE);

    return item;
};

i18n
    .use(initReactI18next)
    .use(LanguageDetector)
    .init({
        fallbackLng: DEFAULT_CULTURE,
        debug: false,
        lowerCaseLng: true,
        react: {
            bindI18n: 'languageChanged',
            bindI18nStore: '',
            transEmptyNodeValue: '',
            transSupportBasicHtmlNodes: true,
            transKeepBasicHtmlNodesFor: ['br', 'strong', 'i', 'p', 'h5', 'b', 'em'],
            useSuspense: false,
        },
        detection: {
            order: ['path', 'localStorage', 'navigator'],
            lookupLocalStorage: 'i18nextLng',
            lookupFromPathIndex: 0,
            caches: ['localStorage'],
        },
        resources: transText,
    });

export default i18n;
