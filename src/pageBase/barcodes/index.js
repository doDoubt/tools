import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import canvasToImage from 'canvas-to-image';
import { SketchPicker } from 'react-color';
import { QRCode } from 'react-qrcode-logo';
import {
    withTranslation,
} from 'react-i18next';

import Enums from '../../enums';
import FileDrop from '../../components/fileUpload';
import Layout from '../../components/layout';
import Toaster from '../../components/message/toaster';
import Sponsor from '../../components/message/sponsor';
import NumberSlider from '../../components/numberSlider';

import './style.scss';

const _gezinImage = '/served/images/qr/icon_square591x591.png';
const _maxBarcodeWidth = 1000;
const _maxLogoWidth = 900;

const _BarcodesPage = ({ t, lang }) => {
    const _quiteZone = 20;

    const [qrKey, _qrKey] = useState(0);
    const _qrKeySet = (val) => _qrKey(val);

    const [bgColor, _bgColor] = useState('#FFFFFF');
    const _bgColorSet = (color) => {
        _bgColor(color);
        _qrKeySet(qrKey + 1);
    };

    const [foreColor, _foreColor] = useState('#000000');
    const _foreColorSet = (color) => {
        _foreColor(color);
        _qrKeySet(qrKey + 1);
    };

    const [qrValue, _qrValue] = useState('https://www.gezincompany.com/');
    const _qrValueSet = (val) => {
        _qrValue(val);
        _qrKeySet(qrKey + 1);
    };

    const [logoOpacity, _logoOpacity] = useState(0.5);
    const _logoOpacitySet = (val) => {
        _logoOpacity(val);
        _qrKeySet(qrKey + 1);
    };

    const [barcodeWidth, _barcodeWidth] = useState(250);
    const _barcodeWidthSet = (val) => {
        _barcodeWidth(Number.parseFloat(val));
        _qrKeySet(qrKey + 1);
    };

    const [logoWidth, _logoWidth] = useState(200);
    const _logoWidthSet = (val) => {
        _logoWidth(Number.parseFloat(val));
        _qrKeySet(qrKey + 1);
    };

    const [qrStyle, _qrStyle] = useState(Enums.QrShapes.dots);
    const _qrStyleSet = (val) => {
        _qrStyle(val);
        _qrKeySet(qrKey + 1);
    };

    const [logoFile, _logoFile] = useState(null);
    const _logoFileSet = (val) => {
        _logoFile(val);
        _qrKeySet(qrKey + 1);
    };

    const [logoType, _logoType] = useState(Enums.QrLogos.gezin);
    const _logoTypeSet = (val) => {
        _logoFileSet(null);
        _logoType(val);
        _qrKeySet(qrKey + 1);
    };

    const [addBorder, _addBorder] = useState(false);
    const _addBorderSet = (val) => {
        _addBorder(val);
        _qrKeySet(qrKey + 1);
    };

    const [borderRad, _borderRad] = useState(15);
    const _borderRadSet = (val) => {
        _borderRad(Number.parseFloat(val));
        _qrKeySet(qrKey + 1);
    };

    const [borderColor, _borderColor] = useState('#000000');
    const _borderColorSet = (val) => {
        _borderColor(val);
        _qrKeySet(qrKey + 1);
    };

    const [lineWidth, _lineWidth] = useState(10);
    const _lineWidthSet = (val) => {
        _lineWidth(Number.parseFloat(val));
        _qrKeySet(qrKey + 1);
    };

    useEffect(() => {
        // eslint-disable-next-line no-use-before-define
        _decoRateBorder();
    }, [qrKey]);

    const _validateForm = () => {
        if (barcodeWidth <= 0) {
            Toaster(
                Enums.ToastTypes.error,
                t('barcodes.error.invalidImageHead'),
                t('barcodes.error.invalidImageMinMsg'),
            );
            return false;
        }

        if (barcodeWidth > _maxBarcodeWidth) {
            Toaster(
                Enums.ToastTypes.error,
                t('barcodes.error.invalidImageHead'),
                t('barcodes.error.invalidImageMaxMsg', { val: _maxBarcodeWidth }),
            );
            return false;
        }

        if (logoWidth <= 0) {
            Toaster(
                Enums.ToastTypes.error,
                t('barcodes.error.invalidLogoHead'),
                t('barcodes.error.invalidLogoMinMsg'),
            );
            return false;
        }

        if (logoWidth > _maxLogoWidth) {
            Toaster(
                Enums.ToastTypes.error,
                t('barcodes.error.invalidLogoHead'),
                t('barcodes.error.invalidLogoMaxMsg', { val: _maxLogoWidth }),
            );
            return false;
        }

        return true;
    };

    const _downloadQrImage = (isPng) => {
        if (!_validateForm()) return;

        canvasToImage(
            document.getElementById('react-qrcode-logo'),
            {
                name: 'doDoubtQr',
                type: isPng ? 'png' : 'jpg',
                quality: 1,
            },
        );
    };

    const _decoRateBorder = () => {
        if (!addBorder) return;

        const ctx = document.getElementById('react-qrcode-logo').getContext('2d');
        const ctxDimension = barcodeWidth + (_quiteZone * 2);
        ctx.strokeStyle = borderColor.hex;
        ctx.lineWidth = lineWidth;

        ctx.beginPath();
        ctx.moveTo(ctxDimension - borderRad, ctxDimension);
        ctx.arcTo(0, ctxDimension, 0, 0, borderRad);
        ctx.arcTo(0, 0, ctxDimension, 0, borderRad);
        ctx.arcTo(ctxDimension, 0, ctxDimension, ctxDimension, borderRad);
        ctx.arcTo(ctxDimension, ctxDimension, 0, ctxDimension, borderRad);
        ctx.stroke();
    };

    const _getImage = () => {
        switch (logoType) {
        case Enums.QrLogos.fb:
            return '/served/images/qr/fb.png';
        case Enums.QrLogos.in:
            return '/served/images/qr/in.png';
        case Enums.QrLogos.ins:
            return '/served/images/qr/ins.png';
        case Enums.QrLogos.twt:
            return '/served/images/qr/twt.png';
        case Enums.QrLogos.custom:
            return logoFile ? URL.createObjectURL(logoFile) : null;
        case Enums.QrLogos.gezin:
        default:
            return _gezinImage;
        }
    };

    const _faIcon = (type) => {
        switch (type) {
        case Enums.QrLogos.fb:
            return 'fab fa-facebook';
        case Enums.QrLogos.in:
            return 'fab fa-linkedin';
        case Enums.QrLogos.ins:
            return 'fab fa-instagram';
        case Enums.QrLogos.twt:
            return 'fab fa-twitter';
        case Enums.QrLogos.gezin:
            return 'fas fa-crown';
        case Enums.QrLogos.custom:
        default:
            return '';
        }
    };

    const _getLogo = (code, name, isActive) => (
        <button
            type="button"
            onClick={() => _logoTypeSet(code)}
            className={isActive ? 'btn btn-dark w-100' : 'btn btn-light w-100'}
        >
            <i className={`${_faIcon(code)} ${isActive ? '  text-light' : ' text-dark'}`} /> {name}
        </button>
    );

    const _label = (id, labelText, cssClass) => (
        <label
            htmlFor={id}
            className={cssClass || 'text-bold text-white'}
        >
            {labelText}
        </label>
    );

    const _sketchPicker = (id, text, func, val) => (
        <div className="col-12 col-md-6 pt-2">
            {
                _label(
                    id,
                    text,
                    'text-bold text-white col-12 text-center',

                )
            }
            <div
                className="d-flex justify-content-center"
            >
                <SketchPicker
                    id={id}
                    name={id}
                    key={id}
                    className="col-12"
                    color={val}
                    onChange={func}
                />
            </div>
        </div>
    );

    return (
        <Layout
            lang={lang}
            pageKey={Enums.PageKeys.barcodes}
        >
            <section className="pt-5 pb-5 mt-5">
                <div className="container h-100">
                    <div className="row">
                        {/* Sponsor */}
                        <Sponsor
                            link="https://www.gezincompany.com/"
                            imageUrl="https://res.cloudinary.com/https-dodoubt-com/image/upload/v1617562606/partner/gezincompany/company/icon_square_colors.png"
                            imageAlt="Gezin Company"
                            message={t('barcodes.sponsorMessage')}
                        />
                        {/* Color pickers */}
                        <div className="col-12 pt-2 d-flex justify-content-center" />
                        {
                            _sketchPicker(
                                'bgColorPicker',
                                t('barcodes.bgColor'),
                                _bgColorSet,
                                bgColor,
                            )
                        }
                        {
                            _sketchPicker(
                                'frColorPicker',
                                t('barcodes.foreColor'),
                                _foreColorSet,
                                foreColor,
                            )
                        }
                        <div className="col-12 col-md-3 pt-2" />
                        {/* Url Selection */}
                        <div className="col-12 pt-2 pb-2">
                            {
                                _label('qrTextData', t('barcodes.textualData'))
                            }
                            <input
                                id="qrTextData"
                                name="qrTextData"
                                className="form-control text-dark"
                                value={qrValue}
                                onChange={(e) => {
                                    if (!e.target || !e.target.value) {
                                        _qrValueSet('');
                                        return;
                                    }
                                    _qrValueSet(e.target.value);
                                }}
                            />
                        </div>

                        {/* Image Width */}
                        <div
                            className="col-12 col-md-6 pt-2 pb-2"
                        >
                            {
                                _label('imageWidth', t('barcodes.imageWidth'))
                            }
                            <input
                                id="imageWidth"
                                name="imageWidth"
                                type="number"
                                step="10"
                                min="100"
                                max="1000"
                                className="form-control text-dark"
                                value={barcodeWidth}
                                onChange={(e) => {
                                    if (!e.target || !e.target.value) {
                                        _barcodeWidthSet(null);
                                        return;
                                    }
                                    _barcodeWidthSet(e.target.value);
                                }}
                            />
                        </div>

                        {/* Image Width */}
                        <div
                            className="col-12 col-md-6 pt-2 pb-2"
                        >
                            {
                                _label('logoWidth', t('barcodes.logoWidth'))
                            }
                            <input
                                id="logoWidth"
                                name="logoWidth"
                                type="number"
                                step="10"
                                min="100"
                                max="1000"
                                className="form-control text-dark"
                                value={logoWidth}
                                onChange={(e) => {
                                    if (!e.target || !e.target.value) {
                                        _logoWidthSet(null);
                                        return;
                                    }
                                    _logoWidthSet(e.target.value);
                                }}
                            />
                        </div>
                        <div className="col-12 pt-2 pb-2">
                            <NumberSlider
                                min={0}
                                max={1}
                                step={0.1}
                                val={logoOpacity}
                                labelText={t('barcodes.opacity', { val: logoOpacity })}
                                id="opacityRange"
                                change={(val) => _logoOpacitySet(val)}
                            />
                        </div>

                        <div className="col-12 pt-2 pb-0">
                            {
                                _label('logoType', t('barcodes.logoType'))
                            }
                        </div>
                        <div className="col-12 pt-0 pb-2">
                            <div id="logoType" className="btn-group">
                                {
                                    _getLogo(Enums.QrLogos.gezin, 'Gezin Co.', logoType === Enums.QrLogos.gezin)
                                }
                                {
                                    _getLogo(Enums.QrLogos.custom, t('barcodes.customLogo'), logoType === Enums.QrLogos.custom)
                                }
                                {
                                    _getLogo(Enums.QrLogos.in, 'LinkedIn', logoType === Enums.QrLogos.in)
                                }
                                {
                                    _getLogo(Enums.QrLogos.fb, 'Facebook', logoType === Enums.QrLogos.fb)
                                }
                                {
                                    _getLogo(Enums.QrLogos.ins, 'Instagram', logoType === Enums.QrLogos.ins)
                                }
                                {
                                    _getLogo(Enums.QrLogos.twt, 'Twitter', logoType === Enums.QrLogos.twt)
                                }
                            </div>
                        </div>
                        {
                            logoType === Enums.QrLogos.custom && (
                                <>
                                    <div className="col-12 pt-2 pb-0">
                                        {
                                            _label('logoFileDrop', t('barcodes.uploadLogo'))
                                        }
                                    </div>
                                    <div className="col-12 pt-0 pb-2">
                                        <div className="btn-group d-flex">
                                            <FileDrop
                                                id="logoFileDrop"
                                                name="logoFileDrop"
                                                fileChanged={(file) => _logoFileSet(file)}
                                                fileRemoved={() => _logoFileSet(null)}
                                                file={logoFile}
                                            />
                                        </div>
                                    </div>
                                </>
                            )
                        }
                        <div className="col-12 pt-2 pb-0">
                            {
                                _label('qrShapeBtn', t('barcodes.qrShape'))
                            }
                        </div>
                        <div className="col-12 pt-0 pb-2">
                            <div className="btn-group d-flex">
                                <button
                                    id="qrShapeBtn"
                                    type="button"
                                    className={`${qrStyle === Enums.QrShapes.dots ? 'btn btn-dark w-100' : 'btn btn-light w-100'}`}
                                    onClick={() => _qrStyleSet('dots')}
                                >
                                    <i className="fas fa-circle" /> {t('barcodes.dot')}
                                </button>
                                <button
                                    type="button"
                                    className={`${qrStyle !== Enums.QrShapes.dots ? 'btn btn-dark w-100' : 'btn btn-light w-100'}`}
                                    onClick={() => _qrStyleSet('squares')}
                                >
                                    <i className="fas fa-square" /> {t('barcodes.square')}
                                </button>
                            </div>
                        </div>

                        <div className="container col-12 p-3 bg-secondary">
                            <div className="row">
                                <div className="col-12 pt-2 pb-0">
                                    {
                                        _label('addBorderBtn', t('barcodes.addBorder'))
                                    }
                                </div>
                                <div className="col-12 pt-0 pb-2">
                                    <div className="btn-group d-flex">
                                        <button
                                            id="addBorderBtn"
                                            type="button"
                                            className={`${addBorder ? 'btn btn-dark w-100' : 'btn btn-light w-100'}`}
                                            onClick={() => _addBorderSet(true)}
                                        >
                                            <i className="far fa-square" /> { t('barcodes.wBorder') }
                                        </button>
                                        <button
                                            type="button"
                                            className={`${!addBorder ? 'btn btn-dark w-100' : 'btn btn-light w-100'}`}
                                            onClick={() => _addBorderSet(false)}
                                        >
                                            <i className="fas fa-square-full" /> { t('barcodes.woBorder') }
                                        </button>
                                    </div>
                                </div>

                                {
                                    addBorder && (
                                        <>
                                            <div className="col-12 col-md-6 pt-2 pb-2">
                                                <NumberSlider
                                                    min={1}
                                                    max={27}
                                                    step={1}
                                                    val={lineWidth}
                                                    labelText={t('barcodes.lineWidth', { val: lineWidth })}
                                                    id="lineWidth"
                                                    change={(val) => _lineWidthSet(val)}
                                                />
                                            </div>
                                            <div className="col-12 col-md-6 pt-2 pb-2">
                                                <NumberSlider
                                                    min={1}
                                                    max={33}
                                                    step={1}
                                                    val={borderRad}
                                                    labelText={t('barcodes.borderRad', { val: borderRad })}
                                                    id="borderRad"
                                                    change={(val) => _borderRadSet(val)}
                                                />
                                            </div>
                                            <div className="col-12 pt-2">
                                                {
                                                    _label('borderColorPicker', t('barcodes.borderCol'))
                                                }
                                                <div
                                                    className="d-flex justify-content-center"
                                                >
                                                    <SketchPicker
                                                        id="borderColorPicker"
                                                        name="borderColorPicker"
                                                        key="borderColorPicker"
                                                        className="col-12"
                                                        color={borderColor}
                                                        onChange={_borderColorSet}
                                                    />
                                                </div>
                                            </div>
                                        </>
                                    )
                                }
                            </div>
                        </div>

                        <div className="col-12 pt-2 pb-0">
                            {
                                _label('svg_item_image', t('barcodes.preview'))
                            }
                        </div>
                        <div className="col-12 pt-2 pb-5 d-flex justify-content-center bg-light">
                            <div
                                id="svg_item_image"
                                className="justify-content-center pt-2 pb-2 m-2"
                                style={{
                                    width: `${barcodeWidth + (_quiteZone * 2)}px`,
                                    height: `${barcodeWidth + (_quiteZone * 2)}px`,
                                }}
                            >
                                <QRCode
                                    key={qrKey}
                                    value={qrValue}
                                    ecLevel="H"
                                    enableCORS={false}
                                    size={barcodeWidth}
                                    quietZone={_quiteZone}
                                    bgColor={bgColor.hex}
                                    fgColor={foreColor.hex}
                                    logoImage={_getImage()}
                                    logoWidth={logoWidth}
                                    logoOpacity={logoOpacity}
                                    qrStyle={qrStyle}
                                />
                            </div>
                        </div>
                        <div className="col-6 pt-5">
                            <div className="d-grid gap-2">
                                <button
                                    type="button"
                                    className="btn btn-light text-bold"
                                    onClick={() => _downloadQrImage(false)}
                                >
                                    {t('barcodes.exportJpg')}
                                </button>
                            </div>
                        </div>
                        <div className="col-6 pt-5">
                            <div className="d-grid gap-2">
                                <button
                                    type="button"
                                    className="btn btn-light text-bold"
                                    onClick={() => _downloadQrImage(true)}
                                >
                                    {t('barcodes.exportPng')}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    );
};

_BarcodesPage.propTypes = {
    t: PropTypes.func.isRequired,
};

_BarcodesPage.propTypes = {
    lang: PropTypes.string.isRequired,
};

const BarcodesPage = withTranslation()(_BarcodesPage);

export default BarcodesPage;
