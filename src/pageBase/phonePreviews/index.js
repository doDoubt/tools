import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    withTranslation,
} from 'react-i18next';
import {
    toPng, toJpeg,
} from 'html-to-image';
import jszip from 'jszip';

import Enums from '../../enums';
import FileDrop from '../../components/fileUpload';
import Layout from '../../components/layout';
import SelectInput from '../../components/selectInput';
import NumberSlider from '../../components/numberSlider';
import ProcessingLoader from '../../components/loader/processingLoader';
import {
    FileHelper,
    StringHelper,
    WindowHelper,
} from '../../utils';

import './style.scss';

const _maxAllowedPreviewCount = 10;

const _defaultDevice = Enums.PhoneTypes.iphonex;
const _defaultScale = 0.5;
const _zipName = 'doDoubt.zip';

const _options = (t) => [
    {
        value: Enums.PhoneTypes.appleWatch,
        label: t('phonePrePage.option.appleWatch'),
        className: 'device-apple-watch',
        styles: [
            {
                scale: 0.25,
                maxHeight: '129px',
                marginTop: '-89px',
                marginLeft: '-64px',
                containerWidth: '212px',
            },
            {
                scale: 0.5,
                maxHeight: '159px',
                marginTop: '-61px',
                marginLeft: '-41px',
                containerWidth: '212px',
            },
            {
                scale: 0.75,
                maxHeight: '189px',
                marginTop: '-31px',
                marginLeft: '-16px',
                containerWidth: '212px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.galaxys8,
        label: t('phonePrePage.option.galaxys8'),
        className: 'device-galaxy-s8',
        styles: [
            {
                scale: 0.25,
                maxHeight: '503px',
                marginTop: '-312px',
                marginLeft: '-131px',
                containerWidth: '390px',
            },
            {
                scale: 0.5,
                maxHeight: '605px',
                marginTop: '-209px',
                marginLeft: '-84px',
                containerWidth: '390px',
            },
            {
                scale: 0.75,
                maxHeight: '710px',
                marginTop: '-105px',
                marginLeft: '-38px',
                containerWidth: '390px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.galaxys8Blue,
        label: t('phonePrePage.option.galaxys8Blue'),
        className: 'device-galaxy-s8 device-blue',
        styles: [
            {
                scale: 0.25,
                maxHeight: '504px',
                marginTop: '-312px',
                marginLeft: '-130px',
                containerWidth: '390px',
            },
            {
                scale: 0.5,
                maxHeight: '606px',
                marginTop: '-210px',
                marginLeft: '-80px',
                containerWidth: '390px',
            },
            {
                scale: 0.75,
                maxHeight: '715px',
                marginTop: '-100px',
                marginLeft: '-35px',
                containerWidth: '390px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.imacPro,
        label: t('phonePrePage.option.imacPro'),
        className: 'device-imac-pro',
        styles: [
            {
                scale: 0.25,
                maxHeight: '287px',
                marginTop: '-180px',
                marginLeft: '-225px',
                containerWidth: '632px',
            },
            {
                scale: 0.5,
                maxHeight: '355px',
                marginTop: '-120px',
                marginLeft: '-148px',
                containerWidth: '632px',
            },
            {
                scale: 0.75,
                maxHeight: '410px',
                marginTop: '-60px',
                marginLeft: '-70px',
                containerWidth: '632px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.iphonex,
        label: t('phonePrePage.option.iphonex'),
        className: 'device-iphone-x',
        styles: [
            {
                scale: 0.25,
                maxHeight: '527px',
                marginTop: '-325px',
                marginLeft: '-147px',
                containerWidth: '440px',
            },
            {
                scale: 0.5,
                maxHeight: '636px',
                marginTop: '-218px',
                marginLeft: '-92px',
                containerWidth: '440px',
            },
            {
                scale: 0.75,
                maxHeight: '745px',
                marginTop: '-109px',
                marginLeft: '-40px',
                containerWidth: '440px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.iphone8,
        label: t('phonePrePage.option.iphone8'),
        className: 'device-iphone-8',
        styles: [
            {
                scale: 0.25,
                maxHeight: '528px',
                marginTop: '-326px',
                marginLeft: '-144px',
                containerWidth: '430px',
            },
            {
                scale: 0.5,
                maxHeight: '640px',
                marginTop: '-218px',
                marginLeft: '-90px',
                containerWidth: '430px',
            },
            {
                scale: 0.75,
                maxHeight: '748px',
                marginTop: '-110px',
                marginLeft: '-40px',
                containerWidth: '430px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.iphone8Gold,
        label: t('phonePrePage.option.iphone8Gold'),
        className: 'device-iphone-8 device-gold',
        styles: [
            {
                scale: 0.25,
                maxHeight: '530px',
                marginTop: '-325px',
                marginLeft: '-140px',
                containerWidth: '430px',
            },
            {
                scale: 0.5,
                maxHeight: '640px',
                marginTop: '-215px',
                marginLeft: '-90px',
                containerWidth: '430px',
            },
            {
                scale: 0.75,
                maxHeight: '750px',
                marginTop: '-110px',
                marginLeft: '-40px',
                containerWidth: '430px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.ipadpro,
        label: t('phonePrePage.option.ipadpro'),
        className: 'device-ipad-pro',
        styles: [
            {
                scale: 0.25,
                maxHeight: '490px',
                marginTop: '-300px',
                marginLeft: '-195px',
                containerWidth: '570px',
            },
            {
                scale: 0.5,
                maxHeight: '590px',
                marginTop: '-200px',
                marginLeft: '-125px',
                containerWidth: '570px',
            },
            {
                scale: 0.75,
                maxHeight: '690px',
                marginTop: '-100px',
                marginLeft: '-55px',
                containerWidth: '570px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.pixel,
        label: t('phonePrePage.option.pixel'),
        className: 'device-google-pixel',
        styles: [
            {
                scale: 0.25,
                maxHeight: '450px',
                marginTop: '-280px',
                marginLeft: '-120px',
                containerWidth: '370px',
            },
            {
                scale: 0.5,
                maxHeight: '545px',
                marginTop: '-185px',
                marginLeft: '-80px',
                containerWidth: '370px',
            },
            {
                scale: 0.75,
                maxHeight: '640px',
                marginTop: '-90px',
                marginLeft: '-30px',
                containerWidth: '370px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.pixelBlack,
        label: t('phonePrePage.option.pixelBlack'),
        className: 'device-google-pixel device-black',
        styles: [
            {
                scale: 0.25,
                maxHeight: '450px',
                marginTop: '-280px',
                marginLeft: '-125px',
                containerWidth: '370px',
            },
            {
                scale: 0.5,
                maxHeight: '545px',
                marginTop: '-185px',
                marginLeft: '-80px',
                containerWidth: '370px',
            },
            {
                scale: 0.75,
                maxHeight: '640px',
                marginTop: '-90px',
                marginLeft: '-35px',
                containerWidth: '370px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.pixelBlue,
        label: t('phonePrePage.option.pixelBlue'),
        className: 'device-google-pixel device-blue',
        styles: [
            {
                scale: 0.25,
                maxHeight: '450px',
                marginTop: '-280px',
                marginLeft: '-122px',
                containerWidth: '370px',
            },
            {
                scale: 0.5,
                maxHeight: '545px',
                marginTop: '-185px',
                marginLeft: '-75px',
                containerWidth: '370px',
            },
            {
                scale: 0.75,
                maxHeight: '640px',
                marginTop: '-90px',
                marginLeft: '-35px',
                containerWidth: '370px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.pixel2xl,
        label: t('phonePrePage.option.pixel2xl'),
        className: 'device-google-pixel-2-xl',
        styles: [
            {
                scale: 0.25,
                maxHeight: '505px',
                marginTop: '-310px',
                marginLeft: '-140px',
                containerWidth: '420px',
            },
            {
                scale: 0.5,
                maxHeight: '650px',
                marginTop: '-200px',
                marginLeft: '-80px',
                containerWidth: '420px',
            },
            {
                scale: 0.75,
                maxHeight: '705px',
                marginTop: '-95px',
                marginLeft: '-55px',
                containerWidth: '420px',
            },
        ],
    },
    {
        value: Enums.PhoneTypes.surfaceStudio,
        label: t('phonePrePage.option.surfaceStudio'),
        className: 'device-surface-studio',
        styles: [
            {
                scale: 0.25,
                maxHeight: '310px',
                marginTop: '-190px',
                marginLeft: '-225px',
                containerWidth: '642px',
            },
            {
                scale: 0.5,
                maxHeight: '365px',
                marginTop: '-125px',
                marginLeft: '-150px',
                containerWidth: '642px',
            },
            {
                scale: 0.75,
                maxHeight: '435px',
                marginTop: '-60px',
                marginLeft: '-70px',
                containerWidth: '642px',
            },
        ],
    },
];

class _PhonePreviewsPage extends Component {
    static propTypes = {
        t: PropTypes.func.isRequired,
        lang: PropTypes.string.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            device: _defaultDevice,
            itemKey: '1',
            logoFiles: [
                null,
            ],
            deviceScale: _defaultScale,
            isProcessing: false,
        };
    }

    componentDidMount() {
        WindowHelper.ScroolToTop();
    }

    _removeLogoFileWithIndex = (index) => {
        const { logoFiles } = this.state;
        this.setState({
            logoFiles: [...logoFiles.filter((_, i) => i !== index)],
            itemKey: StringHelper.GenerateUniqueId(),
        });

        WindowHelper.ScroolToTop();
    };

    _addLogoFileWithIndex = (index, val) => {
        const { logoFiles } = this.state;
        logoFiles[index] = val;
        this.setState({
            logoFiles,
        });

        WindowHelper.ScroolToPosition({
            top: (Number.parseFloat(WindowHelper.GetBodyHeight()) - (WindowHelper.IsSmallBody() ? 1200 : 0)),
            left: 0,
        });
    };

    _setLogoImage = (index, val) => {
        const { logoFiles } = this.state;
        logoFiles[index] = val;
        this.setState({
            logoFiles,
        });
    };

    _label = (id, labelText, cssClass) => (
        <label
            htmlFor={id}
            className={cssClass || 'text-bold text-white'}
        >
            {labelText}
        </label>
    );

    _downloadImageScalingFactor = () => {
        const { deviceScale } = this.state;
        const numDeviceScale = parseFloat(deviceScale, 2);
        return numDeviceScale === 0.50
            ? 0.67 // 0.5 / 0.75
            : numDeviceScale > 0.50
                ? 1 // for 0.75
                : 0.33; // 0.25 / 0.75
    }

    _getFileImage = async (itemIndex, isJpeg) => {
        const elem = document.getElementById(`phone_${itemIndex}`);
        const scalingFactor = this._downloadImageScalingFactor();

        const dataUrl = isJpeg
            ? await toJpeg(
                document.getElementById(`phone_${itemIndex}`),
                {
                // eslint-disable-next-line radix
                    canvasWidth: Number.parseInt(elem.offsetWidth * scalingFactor),
                    // eslint-disable-next-line radix
                    canvasHeight: Number.parseInt(elem.offsetHeight * scalingFactor),
                },
            )
            : await toPng(
                document.getElementById(`phone_${itemIndex}`),
                {
                // eslint-disable-next-line radix
                    canvasWidth: Number.parseInt(elem.offsetWidth * scalingFactor),
                    // eslint-disable-next-line radix
                    canvasHeight: Number.parseInt(elem.offsetHeight * scalingFactor),
                },
            );
        return dataUrl.substring(dataUrl.indexOf('base64,') + 'base64,'.length);
    }

    _getImageName = (numStr, isJpeg) => `preview${numStr}.${isJpeg ? 'jpg' : 'png'}`;

    _saveFiles = async (isJpeg) => {
        WindowHelper.ScroolToTop();

        const { logoFiles } = this.state;

        this.setState({
            isProcessing: true,
        });

        // eslint-disable-next-line new-cap
        const zip = new jszip();

        zip.file(
            this._getImageName('01', isJpeg),
            await this._getFileImage(0, isJpeg),
            {
                base64: true,
            },
        );

        if (logoFiles.length > 1) {
            zip.file(
                this._getImageName('02', isJpeg),
                await this._getFileImage(1, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 2) {
            zip.file(
                this._getImageName('03', isJpeg),
                await this._getFileImage(2, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 3) {
            zip.file(
                this._getImageName('04', isJpeg),
                await this._getFileImage(3, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 4) {
            zip.file(
                this._getImageName('05', isJpeg),
                await this._getFileImage(4, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 5) {
            zip.file(
                this._getImageName('06', isJpeg),
                await this._getFileImage(5, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 6) {
            zip.file(
                this._getImageName('07', isJpeg),
                await this._getFileImage(6, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 7) {
            zip.file(
                this._getImageName('08', isJpeg),
                await this._getFileImage(7, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 8) {
            zip.file(
                this._getImageName('09', isJpeg),
                await this._getFileImage(8, isJpeg),
                {
                    base64: true,
                },
            );
        }

        if (logoFiles.length > 9) {
            zip.file(
                this._getImageName('10', isJpeg),
                await this._getFileImage(9, isJpeg),
                {
                    base64: true,
                },
            );
        }

        FileHelper.saveBlobWithFilename(await zip.generateAsync({ type: 'blob' }), _zipName);

        this.setState({
            isProcessing: false,
        });
    };

    _sectionHeader = (text) => (
        <div className="col-12 text-bold ddColor2 d-flex justify-content-center">
            <h3>{text}</h3>
        </div>
    );

    _previewItem = (t) => {
        const selectedDeviceOption = _options(t).find((x) => x.value === (this.state.device || _defaultDevice));
        const selectedStyle = selectedDeviceOption.styles.find((x) => parseFloat(x.scale, 2) === parseFloat(this.state.deviceScale, 2));

        return (
            [...Array(this.state.logoFiles ? this.state.logoFiles.length || 1 : 1)].map((_, i) => (
                <div key={i} className="row pb-5">
                    {
                        this._sectionHeader(`${t('phonePrePage.visual')} [${i + 1}]`)
                    }
                    <div className="col-12 pt-2 pb-0">
                        {
                            this._label('logoFileDrop', t('phonePrePage.uploadVisual'))
                        }
                    </div>
                    <div className="col-12 pt-0 pb-2" style={{ zIndex: 300 }}>
                        <div id={`idElem${i}`} className="btn-group d-flex">
                            <FileDrop
                                id={`logoFileDrop${i}`}
                                name={`logoFileDrop${i}`}
                                fileChanged={(file) => {
                                    if (file) this._setLogoImage(i, file);
                                }}
                                fileRemoved={() => this._setLogoImage(i, null)}
                                file={this.state.logoFiles && this.state.logoFiles.length >= i + 1 ? this.state.logoFiles[i] : null}
                                label={t('phonePrePage.uploadMsg')}
                            />
                        </div>
                    </div>
                    <div
                        className="col-12"
                        style={selectedStyle}
                    >
                        <div
                            className={`device ${selectedDeviceOption.className}`}
                            style={
                                {
                                    transform: `scale(${this.state.deviceScale})`,
                                }
                            }
                        >
                            <div
                                id={`phone_${i}`}
                                style={{
                                    display: 'inline-block',
                                    width: selectedStyle.containerWidth,
                                    boxSizing: 'border-box',
                                    paddingLeft: '3px',
                                }}
                            >
                                <div className="ml-1">
                                    <div
                                        role="contentinfo"
                                        className="device-frame clearfix"
                                        style={{ overflow: 'hidden' }}
                                        onClick={() => document.getElementById(`logoFileDrop${i}`).click()}
                                    >
                                        <div
                                            className="device-content"
                                            style={{
                                                overflow: 'hidden',
                                                backgroundColor: 'transparent',
                                            }}
                                        >
                                            {
                                                this.state.logoFiles && this.state.logoFiles[i] !== null
                                                && (
                                                    <img
                                                        alt={`${selectedDeviceOption.label} ${t('phonePrePage.preview')}`}
                                                        src={URL.createObjectURL(this.state.logoFiles[i])}
                                                        style={{
                                                            marginTop: '6px',
                                                            paddingLeft: '3px',
                                                            height: '99%',
                                                            width: '99%',
                                                        }}
                                                    />
                                                )
                                            }
                                        </div>
                                    </div>
                                    <div className="device-stripe" />
                                    <div className="device-header" />
                                    <div className="device-sensors" />
                                    <div className="device-btns" />
                                    <div className="device-power" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        className="col-12 mt-4"
                    >
                        <div className="d-flex justify-content-left pl-2 pt-2">
                            <button
                                type="button"
                                disabled={i === 0}
                                onClick={() => this._removeLogoFileWithIndex(i)}
                                className={`btn btn-light ${i === 0 ? 'disabled' : ''}`}
                            >
                                {t('phonePrePage.removePreview')}
                            </button>
                        </div>

                    </div>
                </div>
            ))
        );
    }

    _deviceScaleChange = (val) => this.setState({
        deviceScale: val,
    })

    _deviceScaleLabel = (t) => {
        const { deviceScale } = this.state;
        const numDeviceScale = parseFloat(deviceScale, 2);
        return numDeviceScale === 0.50
            ? t('phonePrePage.scaleMedium')
            : numDeviceScale > 0.50
                ? t('phonePrePage.scaleLarge')
                : t('phonePrePage.scaleSmall');
    }

    render() {
        const {
            t,
            lang,
        } = this.props;

        const {
            itemKey,
            logoFiles,
            deviceScale,
            isProcessing,
        } = this.state;

        const isAddPreviewDiabled = logoFiles && logoFiles.length === _maxAllowedPreviewCount;

        return (
            <Layout
                lang={lang}
                pageKey={Enums.PageKeys.phonePre}
                key={itemKey}
            >
                <section className="pt-5 pb-5 mt-5">
                    <div className="container h-100">
                        {
                            isProcessing && (
                                <ProcessingLoader
                                    message={t('phonePrePage.pleaseWait')}
                                />
                            )
                        }
                        <div>
                            <div className="row">
                                {
                                    this._sectionHeader(t('phonePrePage.options'))
                                }
                                <div
                                    className="col-12 pt-2 pb-2"
                                    style={{ zIndex: 600 }}
                                >
                                    <SelectInput
                                        noOptionsText="No options"
                                        name="device"
                                        label={t('phonePrePage.selectDevice')}
                                        placeholder="placeholder"
                                        onChange={
                                            (val) => this.setState({
                                                device: val,
                                            })
                                        }
                                        value={this.state.device || _defaultDevice}
                                        invalid={false}
                                        invalidMessage={null}
                                        options={
                                            _options(t).map((x) => ({
                                                value: x.value,
                                                label: x.label,
                                            }))
                                        }
                                    />
                                </div>
                                <div
                                    className="col-12 pt-2 pb-2"
                                    style={{ zIndex: 400 }}
                                >
                                    <NumberSlider
                                        min={0.25}
                                        max={0.75}
                                        step={0.25}
                                        val={Number.parseFloat(deviceScale, 2)}
                                        labelText={`${t('phonePrePage.deviceScale')} [${this._deviceScaleLabel(t)}]`}
                                        id="deviceScale"
                                        change={(val) => this._deviceScaleChange(val)}
                                    />
                                </div>
                                <hr className="text-secondary" />
                            </div>
                            {
                                this._previewItem(t)
                            }
                            <div className="row">
                                <div className="col-12 pt-1">
                                    <button
                                        disabled={isAddPreviewDiabled || isProcessing}
                                        type="button"
                                        className={`btn btn-block btn-light ddColor1 text-bold ${isAddPreviewDiabled ? 'disabled' : ''}`}
                                        onClick={() => this._addLogoFileWithIndex(
                                            logoFiles.length,
                                            null,
                                        )}
                                    >
                                        {t('phonePrePage.addPhoneView')} <i className="fas fa-mobile-alt" />
                                    </button>
                                </div>
                                <div className="col-6 pt-1">
                                    <button
                                        disabled={isProcessing}
                                        type="button"
                                        className="btn btn-block btn-light text-bold"
                                        onClick={async () => {
                                            await this._saveFiles(true);
                                        }}
                                    >
                                        {t('phonePrePage.exportFilesJpeg')} <i className="fas fa-image" />
                                    </button>
                                </div>
                                <div className="col-6 pt-1">
                                    <button
                                        disabled={isProcessing}
                                        type="button"
                                        className="btn btn-block btn-light text-bold"
                                        onClick={async () => {
                                            await this._saveFiles(false);
                                        }}
                                    >
                                        {t('phonePrePage.exportFilesPng')} <i className="far fa-image" />
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </Layout>
        );
    }
}

const PhonePreviewsPage = withTranslation()(_PhonePreviewsPage);

export default PhonePreviewsPage;
