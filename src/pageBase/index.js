import React from 'react';
import PropTypes from 'prop-types';
import { StaticImage } from 'gatsby-plugin-image';
import {
    withTranslation,
} from 'react-i18next';

import Enums from '../enums';
import Layout from '../components/layout';

const _IndexPage = ({ t, lang }) => (
    <Layout
        lang={lang}
        pageKey={Enums.PageKeys.index}
    >
        <section>
            <header className="masthead">
                <div className="container d-flex h-100 align-items-center">
                    <div className="mx-auto text-center">
                        <h1 className="mx-auto my-0">doDoubt TooLS</h1>
                        <h2 className="text-white-50 mx-auto mt-2 mb-5">{t('indexPage.desc')}</h2>
                        <a className="btn btn-dark mt-1 mb-2 js-scroll-trigger" href={`/${lang}/barcodes`}>{t('indexPage.barcodeGen')}</a>
                        <br />
                        <a className="btn btn-dark mt-1 mb-2 js-scroll-trigger" href={`/${lang}/phonePreviews`}>{t('indexPage.phoneSkin')}</a>
                    </div>
                </div>
            </header>
        </section>
        <section className="contact-section bg-black">
            <div className="container">
                <div className="row">
                    <hr className="p-0" />
                    <div className="col-6">
                        <div
                            className="d-flex justify-content-center"
                        >
                            <a
                                href="http://tools.appdoubt.com/"
                                className="text-center text-decoration-none p-2 d-flex justify-content-center"
                            >
                                <StaticImage
                                    src="../images/dd.png"
                                    layout="fixed"
                                    width={30}
                                    height={30}
                                    alt="On Premise Deployment"
                                />
                                <span className="text-dark pl-2">On Premise</span>
                            </a>
                        </div>
                    </div>
                    <div className="col-6">
                        <div
                            className="d-flex justify-content-center"
                        >
                            <a
                                href="https://dodoubt.netlify.app/"
                                className="text-center text-decoration-none p-2 d-flex justify-content-center"
                            >
                                <StaticImage
                                    src="../images/netlify.png"
                                    layout="fixed"
                                    width={30}
                                    height={30}
                                    alt="Netlify Deployment"
                                />
                                <span className="text-dark pl-2">Netlify</span>
                            </a>
                        </div>
                    </div>
                    <hr className="p-0" />
                </div>
            </div>
        </section>
    </Layout>
);

_IndexPage.propTypes = {
    t: PropTypes.func.isRequired,
    lang: PropTypes.string.isRequired,
};

const IndexPage = withTranslation()(_IndexPage);

export default IndexPage;
