import React from 'react';
import { toast } from 'react-toastify';

import Enums from '../../enums';

const Toaster = (
    type = Enums.ToastTypes.error,
    header = 'An error has occured',
    message = 'An error has occured',
) => {
    switch (type) {
    case Enums.ToastTypes.success:
        toast.success(
            <div className="row no-gutters">
                <div className="col-12">
                    <b>{header}</b>
                </div>
                <div className="col-12 text-dark border-top pt-1">
                    {message}
                </div>
            </div>,
        );
        return;
    case Enums.ToastTypes.error:
    default:
        toast.error(
            <div className="row no-gutters">
                <div className="col-12">
                    <b>{header}</b>
                </div>
                <div className="col-12 text-dark border-top pt-1">
                    {message}
                </div>
            </div>,
        );
    }
};

export default Toaster;
