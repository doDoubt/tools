import React from 'react';
import PropTypes from 'prop-types';
import {
    withTranslation,
} from 'react-i18next';

const _Sponsor = (props) => (
    <>
        <div className="col-12 pt-2 pb-0 mb-0 text-white d-flex justify-content-center">
            <span>{props.message}</span>
        </div>
        <div className="col-12 pt-0 mt-0 text-white d-flex justify-content-center">
            <a
                href={props.link}
                target="_blank"
                rel="noopener noreferrer"
                className="text-center text-decoration-none p-2"
            >
                <img
                    src={props.imageUrl}
                    alt={props.imageAlt}
                    layout="fixed"
                    width={100}
                    height={100}
                />
            </a>
        </div>
        <hr className="text-secondary" />
    </>
);

_Sponsor.defaultProps = {
    message: 'The project was sponsored by',
};

_Sponsor.propTypes = {
    message: PropTypes.string,
    link: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    imageAlt: PropTypes.string.isRequired,
};

const Sponsor = withTranslation()(_Sponsor);

export default Sponsor;
