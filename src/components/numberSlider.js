import React from 'react';
import PropTypes from 'prop-types';

const NumberSlider = (props) => (
    <>
        <label
            htmlFor={props.id}
            className="text-bold text-white"
        >
            {props.labelText}
        </label>
        <input
            id={props.id}
            name={props.id}
            type="range"
            className="form-range"
            min={props.min}
            max={props.max}
            step={props.step}
            value={props.val || 0}
            onChange={(event) => props.change(event.target.value)}
        />
    </>
);

NumberSlider.defaultProps = {
    min: 0,
    max: Number.MAX_SAFE_INTEGER,
    step: 1,
    val: 0,
};

NumberSlider.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    step: PropTypes.number,
    val: PropTypes.number,
    labelText: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
    change: PropTypes.func.isRequired,
};

export default NumberSlider;
