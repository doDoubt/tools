import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';

const ProcessingLoader = (props) => (
    <div className="row text-light">
        <div className="col-12">
            <div className="text-center">
                <Loader
                    type="Puff"
                    color="#78d1fe"
                    height={100}
                    width={100}
                />
            </div>
            {
                props.message
                && (
                    <h6 className="mt-2 text-center">
                        <small>{props.message}</small>
                    </h6>
                )
            }
        </div>
    </div>
);

ProcessingLoader.propTypes = {
    message: PropTypes.string,
};

ProcessingLoader.defaultProps = {
    message: null,
};

export default ProcessingLoader;
