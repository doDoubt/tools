import * as React from 'react';
import PropTypes from 'prop-types';
import CookieConsent from 'react-cookie-consent';
import { Helmet } from 'react-helmet';
import { ToastContainer } from 'react-toastify';
import {
    withTranslation,
} from 'react-i18next';

import packageJson from '../../../package.json';

import TransSelect from '../../trans/transSelect';

import './layout.scss';
import 'react-toastify/dist/ReactToastify.css';

const _Layout = ({
    t, children, lang, pageKey,
}) => {
    const _infoCard = (
        text,
        linkElem, iconKey,
    ) => (
        <div className="col-md-3 mb-3 mb-md-0">
            <div className="card py-4 h-100">
                <div className="card-body text-center">
                    <i className={`${iconKey} text-dark mb-2`} />
                    <h4 className="text-uppercase m-0">{text}</h4>
                    <hr className="my-3" />
                    <div className="small text-black-50">
                        {linkElem}
                    </div>
                </div>
            </div>
        </div>
    );

    const _alternateLink = (href, hrefLang) =>
        <link key={hrefLang} rel="alternate" href={href} hrefLang={hrefLang} />;

    const _alternateLinks = () => {
        const alts = t(`pageMeta.${pageKey}.alternate`, { returnObjects: true });
        if (!alts || alts.length < 1) return <></>;

        return alts.map((x) => _alternateLink(x.link, x.hreflang));
    };

    return (
        <>
            <Helmet
                htmlAttributes={{
                    lang: lang && lang.length > 1 ? lang.substring(0, 2) : 'en',
                }}
                title={
                    t(`pageMeta.${pageKey}.title`)
                }
            >
                <meta name="google" content="notranslate" />
                <meta name="author" content="doDoubt" />
                <meta property="og:title" content="doDoubt Tools" />
                <meta property="og:type" content="website" />
                <meta property="og:site_name" content="doDoubt Tools" />
                <meta property="og:description" content="A collective list of publicly available tools prepared by doDoubt. Technology solutions for everyone!" />
                <meta property="og:url" content="http://tools.appdoubt.com/" />
                <meta name="image" property="og:image" content="/served/images/icon_square_colors.png" />
                <meta name="description" content={t(`pageMeta.${pageKey}.description`)} />
                <meta name="robots" content={t(`pageMeta.${pageKey}.robots`)} />
                <meta name="googlebot" content={t(`pageMeta.${pageKey}.googlebot`)} />
                <meta name="keywords" content={t(`pageMeta.${pageKey}.keywords`)} />
                <link rel="canonical" href={`https://tools.appdoubt.com/${lang}/`} />
                {
                    _alternateLinks()
                }

                <script
                    src="https://use.fontawesome.com/releases/v5.15.1/js/all.js"
                    crossOrigin="anonymous"
                />
                <link
                    href="https://fonts.googleapis.com/css?family=Varela+Round"
                    rel="stylesheet"
                />
                <link
                    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
                    rel="stylesheet"
                />
                <script
                    src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"
                    integrity="sha384-vtXRMe3mGCbOeY7l30aIg8H9p3GdeSe4IFlP6G8JMa7o7lXvnz3GFKzPxzJdPfGK"
                    crossOrigin="anonymous"
                />
                <link
                    href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
                    crossOrigin="anonymous"
                />
            </Helmet>

            <CookieConsent
                location="bottom"
                buttonText={t('cookie.consent.buttonTxt')}
                cookieName="_ddToolsCon"
                style={{ background: '#3a3a3a' }}
                buttonStyle={{ color: '#4e503b', fontSize: '14px' }}
                expires={365}
            >
                {t('cookie.consent.message')}
            </CookieConsent>
            <section id="page-top">
                <nav className="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
                    <div className="container-fluid">
                        <a className="navbar-brand js-scroll-trigger" href={`/${lang}`}>
                            <span>
                                <span className="ddColor1"><strong>do</strong></span>
                                <span className="ddColor2" style={{ fontWeight: 400 }}>Doubt</span>
                            </span> TooLS
                        </a>
                        <button
                            className="navbar-toggler navbar-toggler-right"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarResponsive"
                            aria-controls="navbarResponsive"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                        >
                            {t('layout.menu')} <i className="fas fa-bars" />
                        </button>
                        <div className="collapse navbar-collapse" id="navbarResponsive">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item"><a className="nav-link js-scroll-trigger" href={`/${lang}/barcodes`}>{t('header.barcodes')}</a></li>
                                <li className="nav-item"><a className="nav-link js-scroll-trigger" href={`/${lang}/phonePreviews`}>{t('header.phoneSkin')}</a></li>
                                <li className="nav-item">
                                    <a href="https://www.dodoubt.com/contact/" target="_blank" rel="noopener noreferrer" className="nav-link js-scroll-trigger">{t('header.contact')}</a>
                                </li>
                                <li className="nav-item">
                                    <TransSelect
                                        lang={lang}
                                    />
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </section>
            {children}
            <section className="contact-section bg-black">
                <div className="container">
                    <div className="row">
                        {
                            _infoCard(
                                t('layout.address'),
                                <a
                                    href="https://www.google.com/maps/place/Kulo%C4%9Flu,+A%C4%9Fa+Hamam%C4%B1+Sk.+No:13"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    className="text-decoration-none text-dark"
                                >
                                    {t('layout.fullAddress')}
                                </a>,
                                'fas fa-map-marked-alt',
                            )
                        }
                        {
                            _infoCard(
                                t('layout.email'),
                                <a
                                    href="http://do@dodoubt.com"
                                    className="text-decoration-none text-dark"
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    do@dodoubt.com
                                </a>,
                                'fas fa-envelope',
                            )
                        }
                        {
                            _infoCard(
                                t('layout.phone'),
                                <a
                                    href="tel:+905324230019"
                                    className="text-decoration-none text-dark"
                                >
                                    +90 532 423 0019
                                </a>,
                                'fas fa-phone-square-alt',
                            )
                        }
                        {
                            _infoCard(
                                t('layout.repo'),
                                <a
                                    href="https://bitbucket.org/doDoubt/tools/src"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    className="text-decoration-none text-dark"
                                >
                                    Bitbucket Repo.
                                </a>,
                                'fab fa-bitbucket',
                            )
                        }
                    </div>
                </div>
            </section>
            <section className="contact-section bg-black">
                <ToastContainer />
            </section>

            <footer className="footer bg-black small text-center text-white-50">
                <script
                    src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
                    integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
                    crossOrigin="anonymous"
                />
                <script
                    src="/served/template.js?v=1"
                />
                <div className="container">
                    <a
                        href="https://www.doDoubt.com"
                        target="_blank"
                        rel="noopener noreferrer"
                        className="text-center text-decoration-none p-2"
                    >
                        <span className="ddColor1"><strong>do</strong></span>
                        <span className="ddColor2" style={{ fontWeight: 400 }}>Doubt</span>
                        <span className="text-dark">{' | '} {packageJson.version}</span>
                    </a>
                </div>
            </footer>
        </>
    );
};

_Layout.propTypes = {
    t: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    lang: PropTypes.string.isRequired,
    pageKey: PropTypes.string.isRequired,
};

const Layout = withTranslation()(_Layout);

export default Layout;
