import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

const SelectInput = (props) => {
    const _change = (selected) => props.onChange(selected && selected.value);

    const _selectOption = () => {
        const index = props.options.findIndex((x) => x.value === props.value);

        if (index < 0) return '';

        return props.options[index];
    };

    const showError = props.invalid && props.validate;

    return (
        <>

            <label
                htmlFor={props.name}
                className={props.labelClass || 'text-bold text-white'}
            >
                {props.label}{props.isRequired && <span className="text-danger pl-1">*</span>}
                {
                    props.description
                    && (
                        <span><br /><small className="opacity-5 pl-0">{props.description}</small></span>
                    )
                }
            </label>
            <Select
                styles={{
                    menu: (provided) => ({ ...provided, zIndex: 999 }),
                }}
                name={props.name}
                id={props.name}
                placeholder={props.placeholder || props.label}
                value={_selectOption()}
                onChange={(selected) => _change(selected)}
                invalid={props.invalid && props.validate}
                maxLength={props.maxlength}
                autoComplete="off"
                isMulti={false}
                options={props.options}
                className={showError ? 'select-error' : ''}
                isClearable
                clearValue={() => props.onChange('')}
                noOptionsMessage={() => props.noOptionsText}
            />
            {
                showError
                && <div className="text-danger dd-v-w-lmt" role="alert"><small>{props.invalidMessage}</small></div>
            }
        </>
    );
};

SelectInput.propTypes = {
    noOptionsText: PropTypes.string,
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    validate: PropTypes.bool,
    invalid: PropTypes.bool.isRequired,
    invalidMessage: PropTypes.string,
    options: PropTypes.array.isRequired,
    description: PropTypes.string,
    labelClass: PropTypes.string,
    maxlength: PropTypes.number,
    isRequired: PropTypes.bool,
};

SelectInput.defaultProps = {
    noOptionsText: 'No options',
    placeholder: 'Please select',
    value: null,
    validate: true,
    invalidMessage: 'Selection is required!',
    description: null,
    labelClass: '',
    maxlength: 50,
    isRequired: false,
};

export default SelectInput;
