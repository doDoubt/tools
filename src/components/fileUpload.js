import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import {
    withTranslation,
} from 'react-i18next';

const _FileDrop = (props) => {
    const maxSize = 2097152;

    const onDrop = useCallback((acceptedFiles) => {
        props.fileChanged(acceptedFiles[0]);
    }, []);

    const _removeImage = () => props.fileRemoved();

    const {
        isDragActive,
        getRootProps,
        getInputProps,
        isDragReject,
        fileRejections,
    } = useDropzone({
        onDrop,
        accept: props.acceptedMimes,
        minSize: 0,
        maxSize,
        multiple: false,
    });

    return (
        <div className="container border rounded border-2 border-secondary text-white text-center">
            <div className="col-12">
                {props.file && (
                    <div className="text-white mt-2 mb-2">
                        {`${props.file.name}`}{' '}
                        <button
                            type="button"
                            className="btn btn-sm btn-danger"
                            onClick={() => _removeImage()}
                        >
                            {props.t('components.fileUpload.removeFile')} <i className="fas fa-trash" />
                        </button>
                    </div>
                )}
            </div>
            <div className="col-12">
                <div id={props.id} {...getRootProps()}>
                    <input {...getInputProps()} />
                    {!isDragActive && (
                        <div className="text-white mt-2 mb-2">
                            {props.label || props.t('components.fileUpload.uploadMsg')}
                        </div>
                    )}
                    {isDragActive && !isDragReject && (
                        <div className="text-white mt-2 mb-2">
                            {props.t('components.fileUpload.draggingMsg')}
                        </div>
                    )}
                    {isDragReject && (
                        <div className="text-danger mt-2 mb-2">
                            {props.t('components.fileUpload.dragRejected')}
                        </div>
                    )}
                    {fileRejections && fileRejections.length > 0 && (
                        <div className="text-danger mt-2 mb-2">
                            {
                                props.t('components.fileUpload.invalidFile', { fileName: fileRejections[0].file.name })
                            }
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

_FileDrop.propTypes = {
    t: PropTypes.func.isRequired,
    fileChanged: PropTypes.func.isRequired,
    fileRemoved: PropTypes.func.isRequired,
    file: PropTypes.object,
    id: PropTypes.string,
    acceptedMimes: PropTypes.string,
    label: PropTypes.string,
};

_FileDrop.defaultProps = {
    file: null,
    id: 'dropInput',
    acceptedMimes: 'image/jpeg, image/png',
    label: null,
};

const FileDrop = withTranslation()(_FileDrop);

export default FileDrop;
