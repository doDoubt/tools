exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
    if (stage === 'build-html') {
        actions.setWebpackConfig({
            module: {
                rules: [
                    {
                        test: /canvas-to-image/,
                        use: loaders.null(),
                    },
                ],
            },
        });
    }
};

exports.onPostBuild = ({ reporter }) => {
    reporter.info('doDoubt Tools has been built!');
};
