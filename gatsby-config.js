module.exports = {
    plugins: [
        'gatsby-plugin-sass',
        'gatsby-plugin-react-helmet',
        'gatsby-plugin-image',
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'images',
                path: `${__dirname}/src/images`,
            },
        },
        'gatsby-transformer-sharp',
        'gatsby-plugin-sharp',
        {
            resolve: 'gatsby-plugin-manifest',
            options: {
                name: 'gatsby-starter-default',
                short_name: 'starter',
                start_url: '/',
                background_color: '#663399',
                theme_color: '#663399',
                display: 'minimal-ui',
                icon: 'src/images/icon.png',
            },
        },
        'gatsby-plugin-gatsby-cloud',
        {
            resolve: 'gatsby-plugin-i18n',
            options: {
                langKeyDefault: 'en',
                useLangKeyLayout: true,
            },
        },
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                name: 'qr_images',
                path: `${__dirname}/src/served`,
            },
        },
        {
            resolve: 'gatsby-plugin-copy-files-enhanced',
            options: {
                source: `${__dirname}/src/served`,
                destination: '/served',
                purge: false,
            },
        },
        {
            resolve: 'gatsby-plugin-google-gtag',
            options: {
                trackingIds: [
                    'UA-194074159-1',
                    '',
                    '',
                ],
            },
        },
    ],
};
